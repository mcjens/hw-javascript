// Set the document title
document.title = 'TODO: Dynamic Fibonacci Sequence in JavaScript';

// Create a red div in the body
var div = document.createElement('div');
div.setAttribute('class', 'red shadowed stuff-box');
document.querySelector('body').appendChild(div);

// Add instructions
var para = document.createElement('p');
para.textContent = "Write the code necessary to add a <form>, an <input> with an onchange attribute, as well as the functions to compute the Fibonacci sequence and output each number as an HTML element";
div.appendChild(para);
